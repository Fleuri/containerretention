# Container Retention - a handy tool to keep your Google Cloud Container registry from bloating

CONTENTS

1. [Motivation](#markdown-header-motivation)
2. [Usage with Kubernetes](#markdown-header-usage-with-kubernetes)
3. [Usage as a docker container](#markdown-header-usage-as-a-docker-container)
4. [Images with tags](#markdown-header-images-with-tags)
5. [Print only docker image](#markdown-header-print-only-docker-image)
6. [Known issues and future development](#markdown-header-known-issues-and-future-development)
7. [Contributing and feedback](#markdown-header-contributing-and-feedback)
8. [License](#markdown-header-license)

## Motivation

GCP Container Registry is a nifty Docker Registry implementation built on top of Google Cloud Storage. While Cloud Storage allows user to set different retention policies for files, currently Container registry does not.

If you are doing automated deployments with Docker Registry with every code commit creating a new docker image to be deployed, you may end up with hundreds of gigabytes worth of docker images of which only a fraction is actually in use any given time. This 

Container Retention is a script intended to run as a cronjob in Kubernetes. It preserves a set number of newest container images and deletes the older ones not within the preservation range.

NOTE: Only images without tags are applicable for deletion. Images with tags are never deleted. See [Images with tags](#markdown-header-images-with-tags) for examples.

## Usage with Kubernetes

To learn about Kubernetes CronJobs, read the [official documentation](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)

To use Container Retention in your GKE cluster, you need to have

1. A Service account with 'Storage Admin' permissions
2. Service Account credentials stored as a secret in the Kubernetes cluster
3. A correctly configured containerRetention.yaml

### Step 1

Create a service account with correct permissions Take a note of its name; It's something like <account name>@<project name>.iam.gserviceaccount.com. Download its private key in JSON format. This will be used when creating a secret.

### Step 2

To create a Kubernetes secret with the key you just downloaded, run `kubectl create secret generic cloud-storage-key --from-file=key.json=<your key name here>`. You can change both 'cloud-storage-key' and 'key.json' to your liking, but I advice against it as then you'll have to make corresponding changes to containerRetention.yaml.

### Step 3

In the last step, you have to add details of your project to containerRetention.yaml. In spec.jobTemplate.spec.spec.containers.env element (Geez!) there are five environment variables:

1. GOOGLE\_APPLICATION\_CREDENTIALS\_PATH
2. REPOSITORY\_PREFIX
3. SERVICE\_ACCOUNT
4. PROJECT\_NAME
5. PRESERVED\_IMAGES

Of these you have to provide values for SERVICE\_ACCOUNT and PROJECT\_NAME. For the former replace <service\_account> with the service account name you created earlier. For the latter replace <project_name> with the ID of your project.

Optionally you can change the value of PRESERVED\_IMAGES. This denotes the number of newest images Container Retention will persist. The default value is 5.  The value has to be a positive integer.

REPOSITORY\_PREFIX denotes the repository subdomain e.g. us.gcr.io, eu.gcr.io or asia.gcr.io. If you want, you can set its value to a subdomain (REPOSITORY\_PREFIX=eu|us|asia) you want to perform the retention operation to. If the value is left empty, the operation will be run for gcr.io. A default value **all** is special: It runs the retention operation for each regional repository as well as gcr.io root domain.

GOOGLE\_APPLICATION\_CREDENTIALS\_PATH tells the script where to find service account credentials. I advice against changing it for reasons expressed previously.

By default Container Retention cronjob runs once a day at midnight. You can change that by modifying the 'schedule' key's value with a cron schedule expression.

### Step 4

Initialize the cronjob by running `kubectl apply -f containerRetention.yaml`. If you want to run the job immediately in addition to waiting until the scheduled time, you can also run `kubectl create job --from=cronjob=container retention <your job name>` which creates a job from the cronjob and executes it immediately.

## Usage as a docker container

You can also use Container Retention as a plain docker container. In that case follow Kubernetes section's steps 1 and 2, but make a new directory for the downloaded key (For example $HOME/storage\_key/) and move the key there. This directory will be used as a volume for the container you'll run. Rename the key to 'key.json'.

For environment variables, follow Kubernetes section's step 3, but make the changes to environment\_variables.txt instead of containerRetention.yaml.

To run the docker container, use the following command:

`docker run -v $HOME/storage_key:/var/secrets/google/ --env-file environment_variables.txt fleuri/container-retention`

In the previous command you added $HOME/storage\_key as a volume which can be found within the container at /var/secrets/google/ . You also provided environment variables as a file.

## Images with tags

By default, Container Retention deletes only untagged images. If an image has a tag, it won't be deleted (unless --force-delete-tags option is added to source code). However, if such image is in the preservation range, it is still counted among preserved images. Consider following examples. Images are in creation order i.e. latest image is first, oldest last. PRESERVED\_IMAGES is set to 5 in every example unless otherwise noted.

1. image:latest
2. image
3. image
4. image
5. image
6. image:some\_tag

All images are preserved

1. image:latest
2. image:some\_tag
3. image
4. image
5. image
6. image

Image 6 is deleted

1. image:latest
2. image
3. image

All images are preserved

PRESERVED\_IMAGES is set to 0.

1. image:latest

Image is preserved as it's tagged 'latest'.

## Print only docker image

If you instead of deleting images wish to just print all image digests (e.g. for testing purposes), I also provide a print-only docker image fleuri/container-retention:print\_only. It disregards PRESERVED\_IMAGES environment variable.

## Known issues and future development

Currently Container Retention parses the NAME header from the image list along with the image names themselves. This pops up as an error when it tries to get digests from an image 'NAME', but it doesn't affect Container Retention's functionality. A similar dismissable error also pops up when trying to delete a tagged image.

* NAME should be filtered from image list and these kind of error messages should be hidden. 
* Prints could be made prettier.
* There could be an option to only perform retention operation to only a subset of repos

## Contributing and feedback

If you wish to contribute to this tool, feel free to raise issues and do pull requests on [Bitbucket](https://bitbucket.org/Fleuri/containerretention/src/master/). You could also go and favourite this project's [docker repository](https://cloud.docker.com/repository/docker/fleuri/container-retention) to approve its legitimacy.

Finally, if you found this tool useful to you or have other feedback, please let me know. You can find my email address in the Dockerfile.

## Contributors

Thank you for /u/distark on Reddit for feedback.

## License

This software is provided under MIT License. See LICENSE.md for details.
