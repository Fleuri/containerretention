#!/usr/bin/env bash

set -eu

#all_prefixes=( us eu asia staging-k8s marketplace l launcher us-mirror eu-mirror asia-mirror mirror k8s );
regional_prefixes=(us eu asia)

for image_name in $(
    if [[ -n ${REPOSITORY_PREFIX} && ${REPOSITORY_PREFIX} == all ]]; then
           gcloud container images list
           for prefix in ${regional_prefixes[*]}; do
               gcloud container images list --repository="${prefix}".gcr.io/"$PROJECT_NAME";
           done;
    elif [ -n "${REPOSITORY_PREFIX}" ]; then
           gcloud container images list --repository="${REPOSITORY_PREFIX}".gcr.io/"$PROJECT_NAME";
    else
           gcloud container images list;
    fi); do

    for digest in $( 
            gcloud container images list-tags "$image_name" --format=json | awk '/digest/{ print $2 }' | sed -e 's/^"//' -e 's/.\{2\}$//'); do
                echo "$image_name"@"$digest";
         done;
    done;
