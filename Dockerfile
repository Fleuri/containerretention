FROM alpine

LABEL maintainer="Lauri 'Fleuri' Suomalainen"
LABEL email="laur1dotsuomalainenatgmaildotcom"

ENV work_dir /app
WORKDIR ${work_dir}

#Google Cloud SDK is incompatible with Python3
RUN apk update && apk add --no-cache python2 curl bash tini

#Downloading gcloud package
RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz

# Installing the package
RUN mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh

# Adding the package path to local
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

ADD . ${work_dir}

ENTRYPOINT ["/sbin/tini", "--", "./docker_entry.sh"]
